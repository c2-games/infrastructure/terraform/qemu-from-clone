resource "proxmox_vm_qemu" "bundle_vm" {
  name        = var.VM_NAME
  desc        = var.VM_DESC
  target_node = var.VM_NODE
  clone       = var.VM_CLONE
  full_clone  = var.VM_FULL_CLONE
  pool        = var.VM_POOL

  agent                  = var.VM_AGENT ? 1 : 0
  cores                  = var.VM_CORES
  sockets                = var.VM_SOCKETS
  vcpus                  = var.VM_VCPUS
  cpu                    = var.VM_CPU
  qemu_os                = var.VM_QEMU_OS
  memory                 = var.VM_MEM
  balloon                = var.VM_MIN_MEM
  scsihw                 = var.VM_SCSI_HW
  boot                   = var.VM_BOOT_ORDER
  vmid                   = var.VM_VMID
  oncreate               = var.VM_START_ON_CREATE
  onboot                 = var.VM_START_ON_BOOT
  define_connection_info = var.VM_DEFINE_CONNECTION_INFO



  dynamic "disk" {
    for_each = var.VM_DISKS
    content {
      size    = lookup(disk.value, "size", null)
      type    = lookup(disk.value, "type", null)
      storage = lookup(disk.value, "name", null)
      cache   = lookup(disk.value, "cache", null)
      backup  = lookup(disk.value, "backup", null)
      ssd     = lookup(disk.value, "ssd", null)
      discard = lookup(disk.value, "discard", null)
    }
  }

  dynamic "network" {
    for_each = var.VM_NETWORKS
    content {
      model     = lookup(network.value, "model", null)
      bridge    = lookup(network.value, "interface", null)
      tag       = lookup(network.value, "tag", null)
      firewall  = lookup(network.value, "firewall", null)
      link_down = lookup(network.value, "link_down", null)
      macaddr   = lookup(network.value, "macaddr", null)
    }
  }
}
