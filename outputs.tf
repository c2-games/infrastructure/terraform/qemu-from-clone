output "proxmox_vm_qemu" {
  description = "Proxmox VM Qemu Resource Attributes"
  value       = proxmox_vm_qemu.bundle_vm
}
