# NCAE CyberGames QEMU from Clone Terraform Module

Simple Proxmox terraform module to clone VMs from templates

## Example usage

```tf
####
# Provider Variables
####

variable "URL" {
  description = "Proxmox API url"
  type        = string
  default     = null
}

variable "TOKEN_NAME" {
  description = "API username with token"
  type        = string
  default     = null
}

variable "TOKEN" {
  description = "API token for authentication"
  type        = string
  sensitive   = true
  default     = null
}

variable "TLS_INSECURE" {
  description = "TLS verification on Proxmox URL"
  type        = bool
  default     = false
}

variable "USERNAME" {
  description = "Username for password authentication"
  type        = string
  default     = null
}

variable "PASSWORD" {
  description = "Password for password authentication"
  type        = string
  sensitive   = true
  default     = null
}

variable "LOG_ENABLE" {
  description = "Enable debug logging"
  type        = bool
  default     = false
}

variable "PROVIDER_LOG_LEVEL" {
  description = "Level of debug to display"
  type        = string
  default     = "debug"
}

variable "PROVIDER_DEBUG" {
  description = "Enable proxmox-api-go debugging"
  type        = bool
  default     = false
}

####
# Module Variables
####
variable "DISKS" {
  type = map(object({
    size = string
    type = string
    name = string
  }))
  default = {
    disk1 = {
      size = "8G"
      type = "virtio"
      name = "local-zfs"
    },
    disk2 = {
      size = "16G"
      type = "scsi"
      name = "local-zfs"
    }
  }
}

variable "NETWORKS" {
  type = map(object({
    model = string
    interface = string
    tag       = optional(number)
    firewall  = optional(bool)
    link_down = optional(bool)
    macaddr   = optional(string)
  }))
  default = {
    i1 = {
      model = "virtio"
      interface = "vmbr0"
    },
    i2 = {
      model = "e1000"
      interface = "vmbr1"
      tag       = 101
      firewall  = true
      link_down = false
      macaddr   = "XX:XX:XX:XX:XX:XX"
    }
  }
}

provider "proxmox" {
  pm_api_url = var.URL

  # Auth with username/password
  pm_user     = var.USERNAME
  pm_password = var.PASSWORD
  # Auth with tokens
  pm_api_token_id     = var.TOKEN_NAME
  pm_api_token_secret = var.TOKEN


  pm_tls_insecure = var.TLS_INSECURE

  pm_log_enable = var.LOG_ENABLE
  pm_debug      = var.PROVIDER_DEBUG
  pm_log_levels = {
    _default    = var.PROVIDER_LOG_LEVEL
    _capturelog = ""
  }
}


module "qemu_from_clone" {
  source = "{{ location to module }}"

  URL = "https://example.example.example:8006/api2/json"
  TOKEN_NAME = "example@pve!example"
  TOKEN = "12345-12345-12345-12345"

  VM_CLONE = "example-template12345"

  VM_NAME = "example-new-vm"
  VM_DESC = "example description"

  VM_NODE = "proxmox-example"
  VM_NETWORKS = var.NETWORKS
  VM_VMID = "12345"

  VM_DISKS = var.DISKS
}
```

<!-- markdownlint-disable MD013 -->
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| proxmox | 2.9.14 |

## Providers

| Name | Version |
|------|---------|
| proxmox | 2.9.14 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [proxmox_vm_qemu.bundle_vm](https://registry.terraform.io/providers/telmate/proxmox/2.9.14/docs/resources/vm_qemu) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VM\_DISKS | Disk map with size, type, and name | ```map(object({ size = string type = string name = string cache = optional(string) backup = optional(string) ssd = optional(number) discard = optional(string) }))``` | n/a | yes |
| VM\_NETWORKS | Network interface map with model and interface | ```map(object({ model = string interface = string tag = optional(number) firewall = optional(bool) link_down = optional(bool) macaddr = optional(string) }))``` | n/a | yes |
| VM\_AGENT | If guest agents is installed or not | `bool` | `false` | no |
| VM\_BOOT\_ORDER | Boot order | `string` | `"order=virtio0;ide2"` | no |
| VM\_CLONE | Lexical reference to VM/template to be cloned | `string` | `null` | no |
| VM\_CORES | Number of VCPU cores to provision the VM with | `number` | `1` | no |
| VM\_CPU | Type of emulated CPU on VM | `string` | `"host"` | no |
| VM\_DEFINE\_CONNECTION\_INFO | Define connection info | `bool` | `null` | no |
| VM\_DESC | Description of cloned VM | `string` | `null` | no |
| VM\_FULL\_CLONE | Toggle the usage of a full clone or linked clone; true enables a full clone | `bool` | `false` | no |
| VM\_INTERFACE | Network interface on VM | `string` | `null` | no |
| VM\_MEM | Amount of memory to allocate to the VM (example: 2048) | `number` | `2048` | no |
| VM\_MIN\_MEM | Minimum mount of memory to allocate to the VM (example: 2048) if ballooning is enabled | `number` | `2048` | no |
| VM\_NAME | Name of the cloned VM | `string` | `null` | no |
| VM\_NODE | Node VM is cloned into | `string` | `null` | no |
| VM\_POOL | Pool the created VM will be placed within | `string` | `null` | no |
| VM\_QEMU\_OS | Type of OS on VM | `string` | `null` | no |
| VM\_SCSI\_HW | The SCSI controller to emulate | `string` | `"virtio-scsi-pci"` | no |
| VM\_SOCKETS | Number of sockets to provision the VM to | `number` | `1` | no |
| VM\_START\_ON\_BOOT | Start VM on boot | `bool` | `null` | no |
| VM\_START\_ON\_CREATE | Start VM on create | `bool` | `null` | no |
| VM\_VCPUS | Number of vCPU cores to provision the VM with (0 = auto, which multiplies VM\_CORES by VM\_SOCKETS) | `number` | `0` | no |
| VM\_VMID | VMID to assign to the VM | `number` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| proxmox\_vm\_qemu | Proxmox VM Qemu Resource Attributes |
<!-- END_TF_DOCS -->
<!-- markdownlint-enable MD013 -->
