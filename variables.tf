####
# Main Variables
####

variable "VM_NAME" {
  description = "Name of the cloned VM"
  type        = string
  default     = null
}

variable "VM_DESC" {
  description = "Description of cloned VM"
  type        = string
  default     = null
}

variable "VM_NODE" {
  description = "Node VM is cloned into"
  type        = string
  default     = null
}

variable "VM_CLONE" {
  description = "Lexical reference to VM/template to be cloned"
  type        = string
  default     = null
}

variable "VM_FULL_CLONE" {
  description = "Toggle the usage of a full clone or linked clone; true enables a full clone"
  type        = bool
  default     = false
}

variable "VM_POOL" {
  description = "Pool the created VM will be placed within"
  type        = string
  default     = null
}

variable "VM_AGENT" {
  description = "If guest agents is installed or not"
  type        = bool
  default     = false
}

variable "VM_CORES" {
  description = "Number of VCPU cores to provision the VM with"
  type        = number
  default     = 1
}

variable "VM_SOCKETS" {
  description = "Number of sockets to provision the VM to"
  type        = number
  default     = 1
}

variable "VM_VCPUS" {
  description = "Number of vCPU cores to provision the VM with (0 = auto, which multiplies VM_CORES by VM_SOCKETS)"
  type        = number
  default     = 0
}

variable "VM_CPU" {
  description = "Type of emulated CPU on VM"
  type        = string
  default     = "host"
}

variable "VM_QEMU_OS" {
  description = "Type of OS on VM"
  type        = string
  default     = null
}

variable "VM_MEM" {
  description = "Amount of memory to allocate to the VM (example: 2048)"
  type        = number
  default     = 2048
}

variable "VM_MIN_MEM" {
  description = "Minimum mount of memory to allocate to the VM (example: 2048) if ballooning is enabled"
  type        = number
  default     = 2048
}

variable "VM_VMID" {
  description = "VMID to assign to the VM"
  type        = number
  default     = null
}

variable "VM_DISKS" {
  description = "Disk map with size, type, and name"
  type = map(object({
    size    = string
    type    = string
    name    = string
    cache   = optional(string)
    backup  = optional(string)
    ssd     = optional(number)
    discard = optional(string)
  }))
}

variable "VM_NETWORKS" {
  description = "Network interface map with model and interface"
  type = map(object({
    model     = string
    interface = string
    tag       = optional(number)
    firewall  = optional(bool)
    link_down = optional(bool)
    macaddr   = optional(string)
  }))
}

variable "VM_SCSI_HW" {
  description = "The SCSI controller to emulate"
  type        = string
  default     = "virtio-scsi-pci"
}

variable "VM_BOOT_ORDER" {
  description = "Boot order"
  type        = string
  default     = "order=virtio0;ide2"
}

variable "VM_INTERFACE" {
  description = "Network interface on VM"
  type        = string
  default     = null
}

variable "VM_START_ON_CREATE" {
  description = "Start VM on create"
  type        = bool
  default     = null
}

variable "VM_START_ON_BOOT" {
  description = "Start VM on boot"
  type        = bool
  default     = null
}

variable "VM_DEFINE_CONNECTION_INFO" {
  description = "Define connection info"
  type        = bool
  default     = null
}
